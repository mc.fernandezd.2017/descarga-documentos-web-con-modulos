from urllib.request import urlopen


class Robot:
    def __init__(self, url):
        self.url = url
        self.descargado = False

    def retrieve(self):  # descarga el documento de la url
        if not self.descargado:
            print(f"Descargando {self.url}")
            urls = urlopen(self.url)
            self.contenido = urls.read().decode('utf-8')  # convierte los bytes en string
            self.descargado = True
        else:
            print(f"Ya se descargó el documento de {self.url} previamente")

    def content(self):  # devuelve una cadena de caracteres con el contenido del documento descargado
        self.retrieve()
        return self.contenido

    def show(self):  # muestra en pantalla el contenido del documento descargado
        print(f"Contenido del documento descargado: {self.content()}")
