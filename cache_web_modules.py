from cache import Cache
from robot import Robot


# Programa principal
if __name__ == '__main__':
    print("CLASE ROBOT")
    url1 = Robot('http://gsyc.urjc.es/')
    url2 = Robot('https://www.aulavirtual.urjc.es')
    url1.content()
    url1.retrieve()
    url2.retrieve()
    url2.content()
    url1.show()

    print("CLASE CACHE")
    u1 = Cache()
    u2 = Cache()
    u1.retrieve('https://www.aulavirtual.urjc.es')
    u2.show('http://gsyc.urjc.es/')
    u1.show_all()
    u2.show_all()

