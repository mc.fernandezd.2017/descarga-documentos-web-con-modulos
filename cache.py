from robot import Robot


class Cache:
    def __init__(self):
        self.cache = {}  # diccionario vacío

    def retrieve(self, url):  # descarga el documento correspondiente a esa url
        if url not in self.cache:
            self.cache[url] = Robot(url)
        else:
            print("Ya ha sido descargado previamente")

    def content(self, url):  # devuelve una cadena de caracteres con el contenido del documento correspndiente a la url
        self.retrieve(url)
        return self.cache[url].content()

    def show(self, url):  # muestra en pantalla el contenido del documento correspondiente con esa url
        print(f"Contenido del documento correspondiente a la url: {self.content(url)}")

    def show_all(self):  # muestra un listado de todas las urls cuyo documento se ha descargado ya
        for url in self.cache:
            print(f"Url cuyo documento ya se ha descargado: {url}")

